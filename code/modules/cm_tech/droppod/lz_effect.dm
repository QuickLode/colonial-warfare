/obj/effect/warning
	name = "warning"
	icon = 'icons/effects/alert.dmi'
	icon_state = "alert_greyscale"
	anchored = TRUE

	layer = ABOVE_OBJ_LAYER

/obj/effect/warning/droppod
	name = "droppod landing-zone"
	icon_state = "techpod_lz_marker"

/obj/effect/warning/alien
	name = "alien warning"
	color = "#a800ff"
